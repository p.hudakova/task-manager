.PHONY: test
unit-test:
	nosetests

.PHONY: test-coverage
unit-test-coverage:
	nosetests --with-coverage

.PHONY: run
run:
	python main.py
